﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.IO;
using ExitGames.Client.Photon;
using Unity.Collections;
using UnityEngine.UI;


public class PlayerManager : MonoBehaviourPunCallbacks , IOnEventCallback 
{
    PhotonView PV;

    public readonly byte CallEvent;

    

    GameObject controller;

    public GameObject CanvasGameover;
    float Life ;

    
    
    
     void Awake()
     {
         PV = GetComponent<PhotonView>();
     }

    // Start is called before the first frame update
    void Start()
    {
        Life = 10;
        if (PV.IsMine)
        {
            CreateController();
        }
    }

    void CreateController()
    {
        Life -= 1;
        Debug.Log("Life : "+Life);
        if (Life >0)
        {
            Transform spawnpoint = SpawnManager.Instance.GetSpawnpoint();
            controller =  PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerController"), spawnpoint.position, spawnpoint.rotation,0, new object[]{PV.ViewID});
        }

        if (Life <= 0)
        {
            IsgameOver();
            StartCoroutine(Gameover());
        }
        
        
        
        
    }

    

    public void Die()
    {
        PhotonNetwork.Destroy(controller);
        CreateController();
    }
    
    public void IsgameOver()
    {
        CanvasGameover.SetActive(true);
    }
    
    public void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == CallEvent)
        {
            IsgameOver();
        }
    }
      
    public IEnumerator Gameover()
    {
        yield return new WaitForSeconds(0.1f);
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
        ExitGames.Client.Photon.SendOptions sendOptions=new ExitGames.Client.Photon.SendOptions{Reliability = true};
        PhotonNetwork.RaiseEvent(CallEvent, null, raiseEventOptions, sendOptions);
        
    }

    


    
}
