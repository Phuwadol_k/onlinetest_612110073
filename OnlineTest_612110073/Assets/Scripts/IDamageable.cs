﻿using Photon.Pun;
public interface IDamageable
{
    void TakeDamage(float damage );
     
}